#include <stdio.h>
#include <stdlib.h>

#define H(k,n) k%n
#define D(k,n,c) (c-k%n)
//linear probing
int * hashtable(int n){
    int * hash = (int *)malloc(n*sizeof (int));
    for (int i = 0; i < n; ++i) {
        hash[i] = -1;
    }
    return hash;
}
char isempty(const int * hash, int i){
    return (char)(*(hash + i) == -1);
}
void insert (const int k , int * hash,const int n,const int c){
    int hashval = H(k,n);
    if(isempty(hash,hashval)){
        *(hash+hashval) = k;
    }else {
        int hashval2 = D(k,n,c);
        do {
            hashval2 += hashval;
            hashval2 %= n;
        }while (!isempty(hash,hashval2));
        *(hash+hashval2) = k;
    }
}
int lookup(int* hash, int k, int n, int c){
    int hashval = H(k,n);
    if(isempty(hash,hashval)){
        return -1;
    } else{
        if(*(hash+hashval) == k){
            return hashval;
        }else {
            int hashval2 = D(k,n,c);
            do {
                hashval2 += hashval;
                hashval2 %= n;
                if(*(hash+hashval2) == k)
                    return hashval2;
            }while (!isempty(hash,hashval2));
            return -1;
        }
    }
}
void printarr(int* arr,int n){
    printf("index\tkey\n");
    for (int i = 0; i < n; ++i) {
        int value = *(arr+i);
        if(value == -1){
            printf("%d \t\t %s\n",i,"--");
        }else{
            printf("%d \t\t %d\n",i,*(arr+i));
        }
    }
}
int main() {
    int n=13;int c =7;
    int arr[] = {18,41,22,44};
    int * hash = hashtable(n);
    for (int i = 0; i < 4; ++i) {
        insert(arr[i],hash,n,c);
    }
    printarr(hash,n);
    int k = 44;
    int search = lookup(hash,k,n,c);
    if (search == -1){
        printf("\n%d NOT FOUND IN HASH TABLE\n",k);
    }else{
        printf("\n%d IS LOCATED IN INDEX %d\n",k,search);
    }
}