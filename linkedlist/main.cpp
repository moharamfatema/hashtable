#include <stdio.h>
#include <stdlib.h>
#define FALSE 0
#define TRUE 1
#define n 13
#define H(k) k%n
//typedef u_int8_t bool;
/*linked list implementation*/
#ifndef LINKEDLIST_HEADER
#define LINKEDLIST_HEADER

typedef struct node Node;

typedef struct list List;

List * makelist();
void add(int data, List * list);
void delete_(int data, List * list);
void display(List * list);
void reverse(List * list);
void destroy(List * list);

#endif

struct node {
    int data;
    struct node * next;
};

struct list {
    Node * head;
};

Node * createnode(int data);

Node * createnode(int data){
    Node * newNode = (Node *)malloc(sizeof(Node));
    if (!newNode) {
        return NULL;
    }
    newNode->data = data;
    newNode->next = NULL;
    return newNode;
}

List * makelist(){
    List * list = (List *) malloc(sizeof(List));
    if (!list) {
        return NULL;
    }
    list->head = NULL;
    return list;
}

void display(List * list) {
    Node * current = list->head;
    if(list->head == NULL)
        return;

    for(; current != NULL; current = current->next) {
        printf(" \t %d\n", current->data);
    }
}

void add(int data, List * list){
    Node * current = NULL;
    if(list->head == NULL){
        list->head = createnode(data);
    }
    else {
        current = list->head;
        while (current->next!=NULL){
            current = current->next;
        }
        current->next = createnode(data);
    }
}

void delete_(int data, List * list){
    Node * current = list->head;
    Node * previous = current;
    while(current != NULL){
    if(current->data == data){
    previous->next = current->next;
    if(current == list->head)
    list->head = current->next;
    free(current);
return;
}
previous = current;
current = current->next;
}
}

void reverse(List * list){
    Node * reversed = NULL;
    Node * current = list->head;
    Node * temp = NULL;
    while(current != NULL){
        temp = current;
        current = current->next;
        temp->next = reversed;
        reversed = temp;
    }
    list->head = reversed;
}

void destroy(List * list){
    Node * current = list->head;
    Node * next = current;
    while(current != NULL){
        next = current->next;
        free(current);
        current = next;
    }
    free(list);
}


/*hash table*/



typedef int TYPE;

typedef struct  hash_cell {
    bool full;
    List *data;
}hash_cell;

hash_cell *hash_table(){
    auto* hashtable = (hash_cell *) malloc(n * sizeof (hash_cell));
    for (int i = 0; i < n; ++i) {
        List* list = makelist();
        hashtable[i].data = list;
        hashtable[i].full = FALSE;

    }
    return hashtable;
}
bool is_empty(hash_cell *hash1, int i){
    return !hash1[i].full;
}

void insert(hash_cell* hash, TYPE val){
    int index = H(val);
    add(val,(hash[index]).data);
    hash[index].full = TRUE;
}
int search(TYPE val,hash_cell* hash ){

    for (int i = 0; i < n; ++i) {
        if (!is_empty(hash,i)){
            Node* nextnode = hash[i].data->head;
            while (nextnode){
                if(val == nextnode->data){
                    return i;
                }nextnode = nextnode->next;
            }
        }
    }return -1;
}
void print_hash(hash_cell* hash){
    printf("index \t value\n");
    for (int i = 0; i < n; ++i) {
        if (hash[i].full == TRUE){
            printf("%d",i);
            display(hash[i].data);
        } else{
            printf("%d \t --\n",i);
        }
    }
}
void destroyhash(hash_cell* hash){
    for (int i = 0; i < n; ++i) {
        if(!is_empty(hash,i)){
            destroy(hash[i].data);
            hash[i].full = FALSE;
        }
    }
    free(hash);
}
int main() {
    TYPE arr[] = {18,41,22,44};
    hash_cell * hashtable = hash_table();
    for (int i = 0; i < 4; ++i) {
        insert(hashtable,arr[i]);
    }
    print_hash(hashtable);

    int index = search(44,hashtable);
    if (index != -1){
        printf("\n%d is found at index %d\n",44,index);
    }else{
        printf("value %d is not found in hashtable",44);
    }

    destroyhash(hashtable);
    return 0;
}
